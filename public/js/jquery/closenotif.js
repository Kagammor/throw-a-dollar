function closeNotification()
{
	// Slide up notification message and go to homepage
	$('#status').slideUp('fast', function() {
		parent.location = '/';
	});
	
	// Hide close button itself as well
	$('#status_close').hide();
}
