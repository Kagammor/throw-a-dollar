<?php
	include "functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Throw a Dollar</title>
		
		<link href="public/css/main.css" type="text/css" rel="stylesheet">
		<link href="public/css/info.css" type="text/css" rel="stylesheet">
		
		<script src="public/js/jquery/jquery.js"></script>
	</head>

	<body>
		<div id="header">
			<div id="header_container">
				<a href="/"><img src="public/img/icons/empty.png" id="header_logo"></a>
			</div>
		</div>
		
		<div id="container">
			<h2>Team</h2>
			
			<div class="teammember">
				<img src="public/img/photos/team/niels.jpg" alt="Niels Simenon">
				<p class="name">Niels Simenon</p>
				<p class="title">Founder/CEO</p>
			</div>
			
			<div class="teammember">
				<img src="public/img/photos/team/rutger.jpg" alt="Rutger Rauws">
				<p class="name">Rutger Rauws</p>
				<p class="title">CTO/CVO</p>
			</div>
			
			<div class="teammember">
				<img src="public/img/photos/team/narita.jpg" alt="Narita Derks">
				<p class="name">Narita Derks</p>
				<p class="title">Chief PR</p>
			</div>
		</div>
	</body>
</html>
