<?php
	include "functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Throw a Dollar</title>
		
		<link href="public/css/main.css" type="text/css" rel="stylesheet">
		
		<script src="public/js/jquery/jquery.js"></script>
		<script src="public/js/jquery/closenotif.js"></script>
		<script type="text/javascript" src="http://www.mollie.nl/partners/js/803677.paybox.js"></script> <!-- Mollie -->
	</head>

	<body>
		<div id="fb-root"></div>
		<script src="public/js/facebook.js"></script>
	
		<!-- Status feedback -->
		<?php
			statusFeedback();
		?>
		
		<div id="header">
			<div id="header_container">
				<a href="/"><img src="public/img/icons/empty.png" id="header_logo"></a>
			</div>
		</div>
			
		<section id="content">
			<section id="text">
				<p>We are a small organisation with a rather noticeable tagline:</p>
				<blockquote id="tagline">Donate $1, and we won't necessarily use it for charity.</blockquote>
				<p>Donate a symbolic amount if you are getting tired of organisations unjustly claiming to donate your money to charity.</p>
				<p>We will use your dollar for any given purpose we have in mind, and admit that for once.</p>
				</section>
							
			<section id="donation">	
				<button id="donate" onclick="$('#pay').slideToggle('fast'); $('#stats').slideToggle('fast');">Donate $1</button>
				
				<div id="pay">
					<!-- PayPal -->
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="BSTRFY6DEYK9Q">
						<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					</form>
					PayPal or credit card
						
					<!-- Mollie -->
					<p><a href="#" onClick="mbetaal('id=1090707');return false;"><img src="public/img/logos/mollie.png"></a></p>
					<p>Text or call (EU only)</p>
				</div>
			
				<footer id="stats">
					<p id="amount">$0</p>
					<p id="session">1st session</p>
				</footer>
			</section>
		</section>
		
		<footer id="footer"><a href="team.php">Team</a> | <a href="#">Privacy</a> | <a href="#">Legal</a> | &copy; <?php echo date('Y'); ?></footer>
	</body>
</html>
